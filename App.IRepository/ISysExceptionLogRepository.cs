﻿using App.Core;
using App.Entities;

namespace App.IRepository
{
    public interface ISysExceptionLogRepository : IBaseRepository<SysExceptionLog>
    {

    }
}
