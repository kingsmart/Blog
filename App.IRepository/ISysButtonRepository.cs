﻿using App.Entities;
using App.Core;

namespace App.IRepository
{
    public interface ISysButtonRepository : IBaseRepository<SysButton>
    {

    }
}
