﻿using System;
using System.Collections.Generic;
using System.Text;
using App.Entities;

namespace App.IServices
{
    public interface IArticleCategoryLogic : IBaseLogic<ArticleCategory>
    {
    }
}
